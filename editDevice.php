<?php
require('inc_db.php');
global $mysqli;
?>

<div class="container-fluid">
<?php
$sql = "
SELECT *
FROM device
WHERE device_id = " . $_REQUEST['deviceId'];
if ($result = $mysqli->query($sql)) {
	$row = $result->fetch_assoc();
	$deviceId = $row['device_id'];

	// Build the array of Applications
	$applicationSql = "SELECT application_id FROM device_application WHERE device_id = " . $deviceId;
	$applicationResult = $mysqli->query($applicationSql);
	$applicationRows = $applicationResult->fetch_all(MYSQLI_NUM);
	$applicationArr = array();
	foreach($applicationRows as $applicationRow) {
		$applicationArr[] = $applicationRow[0];
	}
	
	// Build the array of Exceptions
	$exceptionSql = "SELECT exception_id FROM device_exception WHERE device_id = " . $deviceId;
	$exceptionResult = $mysqli->query($exceptionSql);
	$exceptionRows = $exceptionResult->fetch_all(MYSQLI_NUM);
	$exceptionArr = array();
	foreach($exceptionRows as $exceptionRow) {
		$exceptionArr[] = $exceptionRow[0];
	}
?>
	<form id="editForm" method="post">
  <input type="hidden" name="device_id" value="<?php echo $deviceId ?>">
  <div class="row">
  	<div class="col-sm-6" style="border-right: 1px dotted #ccc">
<?php
require 'inc_deviceDetails.php';
?>
    </div>
    <div class="col-sm-6">
<?php 
require 'inc_deviceNotes.php';
?>    
		</div>    
  </div>    
  <div class="row" style="border-top: 1px dotted #ccc; padding-top: 20px">
  	<div class="col-sm-12">
    	<div class="pull-right">
        <button type="button" class="btn btn-primary-outline" onClick="jQuery('#dialog').dialog('close');">Cancel</button>&nbsp;
        <input class="btn btn-primary" type="submit" value="Update Device">
      </div>
    </div>
  </div>
  </form>
<?php
}	
?>
<script>
jQuery(document).ready( function($) {
	$('[name="<?php echo $_REQUEST['f'] ?>"]').focus();	


  // Attach a submit handler to the form
  $("#editForm").on("submit", function( event ) {
	  
	  notesSuccess = true;
	  detailsSuccess = true;
	  
	  // stop the form from submitting the normal way and refreshing the page
	  event.preventDefault();
  
	  // get the form data
	  // there are many ways to get this data using jQuery (you can use the class or id also)
	  var formData = $("#editForm").serialize();
  
	  // process the form for details
	  $.ajax({
			  type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
			  url         : 'processEditDevice.php', // the url where we want to POST
			  data        : formData, // our data object
			  dataType    : 'json', // what type of data do we expect back from the server
			  encode      : true
	  })
			  // using the done promise callback
			  .done(function(detailsData) {
	  
					  // log data to the console so we can see
					  // console.log(detailsData); 
  
					  if (detailsData['detailsSuccess'] === false) {
						  detailsSuccess = false;
					  }
			  });
  
  
	  // process the form for notes
	  $.ajax({
			  type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
			  url         : 'processEditDeviceNotes.php', // the url where we want to POST
			  data        : formData, // our data object
			  dataType    : 'json', // what type of data do we expect back from the server
			  encode      : true
	  })
			  // using the done promise callback
			  .done(function(notesData) {
	  
					  // log data to the console so we can see
					  // console.log(notesData); 
  
					  if (notesData['notesSuccess'] === false) {
						  notesSuccess = false;
					  }
  
			  });
  
  
  
	  if ((notesSuccess == true) && (detailsSuccess == true)) {
		  // Reload the parent table
		  deviceTable.ajax.reload(null, false);
		  //deviceTable.ajax.url('api-dt-listDevices.php').load(null, false);
		  
		  // Close the dialog
		  $("#dialog" ).dialog("close");
	  }
	  
  });

});

</script>
</div>
