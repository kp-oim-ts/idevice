<?php
require('inc_db.php');
global $mysqli;
?>

<div class="container-fluid">
<?php
$deviceId = $_REQUEST['deviceId'];
?>
	<form id="editForm" method="post">
  <input type="hidden" name="device_id" value="<?php echo $deviceId ?>">
  <div class="row">
    <div class="col-sm-12">
<?php 
include 'inc_deviceNotes.php';
?>    
		</div>    
  </div>    
	<div class="row" style="border-top: 1px dotted #ccc; padding-top: 20px">
  	<div class="col-sm-12">
    	<div class="pull-right">
        <button type="button" class="btn btn-primary-outline" onClick="jQuery('#dialog').dialog('close');">Cancel</button>&nbsp;
        <input class="btn btn-primary" type="submit" value="Update Device Notes">
      </div>
    </div>
  </div>
  </form>
</div>
<script>
jQuery(document).ready( function($) {

  // Attach a submit handler to the form
  $("#editForm").on("submit", function( event ) {
	  // stop the form from submitting the normal way and refreshing the page
	  event.preventDefault();
  
	  // get the form data
	  // there are many ways to get this data using jQuery (you can use the class or id also)
	  var formData = $("#editForm").serialize();
  
	  // process the form
	  $.ajax({
			  type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
			  url         : 'processEditDeviceNotes.php', // the url where we want to POST
			  data        : formData, // our data object
			  dataType    : 'json', // what type of data do we expect back from the server
			  encode      : true
	  })
			  // using the done promise callback
			  .done(function(notesData) {
	  
					  // log data to the console so we can see
					  // console.log(notesData); 
  
					  if (notesData['notesSuccess'] === true) {
						  // Reload the parent table
						  deviceTable.ajax.reload(null, false);
						  
						  // Close the dialog
						  $("#dialog" ).dialog("close");
					  }
			  });
	  
  });
});
</script>
