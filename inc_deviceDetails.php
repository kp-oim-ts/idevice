<?php
/*
	inc_deviceDetails.php
	Requires $deviceId and $exceptionArr to be defined prior to inclusion
*/

$deviceTypeId = isset($row['device_type_id']) ? $row['device_type_id'] : null;
$deviceModel = isset($row['device_model']) ? $row['device_model'] : "";
$deviceCarrier = isset($row['device_carrier']) ? $row['device_carrier'] : "";
$deviceIdentifier1 = isset($row['device_identifier1']) ? $row['device_identifier1'] : "";
$deviceIdentifier2 = isset($row['device_identifier2']) ? $row['device_identifier2'] : "";
$deviceProject = isset($row['device_project']) ? $row['device_project'] : "";
$deviceStatusId = isset($row['device_status_id']) ? $row['device_status_id'] : null;
$ownerNuid = isset($row['owner_nuid']) ? $row['owner_nuid'] : "";
$ownerFname = isset($row['owner_fname']) ? $row['owner_fname'] : "";
$ownerLname = isset($row['owner_lname']) ? $row['owner_lname'] : "";
$ownerEmail = isset($row['owner_email']) ? $row['owner_email'] : "";
$ownerRole = isset($row['owner_role']) ? $row['owner_role'] : "";
$ownerMedctr = isset($row['owner_medctr']) ? $row['owner_medctr'] : "";
$ownerLocation = isset($row['owner_location']) ? $row['owner_location'] : "";
$ownerDept = isset($row['owner_dept']) ? $row['owner_dept'] : "";
$attestationOnFile = isset($row['attestation_on_file']) ? $row['attestation_on_file'] : "";
?>
     	<h4>Device</h4>
     	<div class="row">
        <div class="col-sm-4">
          <fieldset class="form-group">  
            <label for="owner_fname">Device Type</label>
            <select class="form-control" name="device_type_id">
<?php
	//$selDeviceType = $row['device_type_id'];
	$dTypeSql = "SELECT * FROM device_type";
	if ($dTypeResult = $mysqli->query($dTypeSql)) {
		while ($dTypeRow = $dTypeResult->fetch_assoc()) {
?>
						<option value="<?php echo $dTypeRow['device_type_id'] ?>" <?php echo ($deviceTypeId == $dTypeRow['device_type_id'] ? "selected" : "") ?>><?php echo $dTypeRow['name'] ?></option>
<?php
		}
	}
?>		
            </select>
          </fieldset>
        </div>  
        <div class="col-sm-4">
          <fieldset class="form-group">  
            <label for="device_model">Device Model <?php echo $deviceModel ?></label>
            <input type="text" class="form-control" name="device_model" value="<?php echo $deviceModel ?>">
          </fieldset>
        </div>
        <div class="col-sm-4">
          <fieldset class="form-group">  
            <label for="device_carrier">Carrier</label>
            <input type="text" class="form-control" name="device_carrier" value="<?php echo $deviceCarrier ?>">
          </fieldset>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <fieldset class="form-group">  
            <label for="device_identifier1">Identifier 1</label>
            <input type="text" class="form-control" name="device_identifier1" value="<?php echo $deviceIdentifier1 ?>">
          </fieldset>
        </div>  
        <div class="col-sm-6">
          <fieldset class="form-group">  
            <label for="device_identifier2">Identifier 2</label>
            <input type="text" class="form-control" name="device_identifier2" value="<?php echo $deviceIdentifier2 ?>">
          </fieldset>
        </div>
      </div>    
     	<div class="row">
        <div class="col-sm-6">
          <fieldset class="form-group">  
            <label for="device_project">Project/Phase</label>
            <input type="text" class="form-control" name="device_project" value="<?php echo $deviceProject ?>">
          </fieldset>
        </div>  
        <div class="col-sm-6">
          <fieldset class="form-group">  
            <label for="device_status_id">Status</label>
            <select class="form-control" name="device_status_id">
            <option value=""></option>
<?php
	//$selStatus = $row['device_status_id'];
	$statusSql = "SELECT * FROM device_status";
	if ($statusResult = $mysqli->query($statusSql)) {
		while ($statusRow = $statusResult->fetch_assoc()) {
?>
						<option value="<?php echo $statusRow['device_status_id'] ?>" <?php echo ($deviceStatusId == $statusRow['device_status_id'] ? "selected" : "") ?>><?php echo $statusRow['name'] ?></option>
<?php
		}
	}
?>		
            </select>
          </fieldset>
        </div>  
      </div>  
      <div class="row">  
        <div class="col-sm-12">
          <fieldset class="form-group">  
            <label for="application_id[]">Applications</label>
            <select class="form-control" name="application_id[]" id="selApplicationId" multiple>
<?php
	$applicationSql = "SELECT * FROM application ORDER BY name";
	if ($applicationResult = $mysqli->query($applicationSql)) {
		while ($applicationRow = $applicationResult->fetch_assoc()) {
?>
						<option value="<?php echo $applicationRow['application_id'] ?>" <?php if ((!empty($applicationArr)) && (in_array($applicationRow['application_id'], $applicationArr))) { echo "selected"; } ?>><?php echo $applicationRow['name'] ?></option>
<?php
		}
	}
?>		
            </select>
          </fieldset>
        </div>  
      </div>  
      <div class="row">  
        <div class="col-sm-12">
          <fieldset class="form-group">  
            <label for="exception_id[]">Flags</label>
            <select class="form-control" name="exception_id[]" id="selExceptionId" multiple>
<?php
	//$selException = $row['exception_id'];
	$exceptionSql = "SELECT * FROM exception ORDER BY name";
	if ($exceptionResult = $mysqli->query($exceptionSql)) {
		while ($exceptionRow = $exceptionResult->fetch_assoc()) {
?>
						<option value="<?php echo $exceptionRow['exception_id'] ?>" <?php if ((!empty($exceptionArr)) && (in_array($exceptionRow['exception_id'], $exceptionArr))) { echo "selected"; } ?>><?php echo $exceptionRow['name'] ?></option>
<?php
		}
	}
?>		
            </select>
          </fieldset>
        </div>  
      </div>  
     	<h4>Owner</h4>
      <div class="row">
        <div class="col-sm-6">
          <fieldset class="form-group">  
            <label for="owner_nuid">NUID</label>
            <input type="text" class="form-control" name="owner_nuid" value="<?php echo $ownerNuid ?>">
          </fieldset>
        </div>  
        <div class="col-sm-6">
          <label for="owner_attestation">Attestation</label>
          <div class="c-inputs-stacked">
            <label class="c-input c-radio">
              <input id="radioStacked1" name="attestation_on_file" type="radio" value="1" <?php echo $attestationOnFile == 1 ? "checked" : "" ?>>
              <span class="c-indicator"></span>
              Yes
            </label>
            <label class="c-input c-radio">
              <input id="radioStacked2" name="attestation_on_file" type="radio" value="0" <?php echo empty($attestationOnFile) ? "checked" : "" ?>>
              <span class="c-indicator"></span>
              No
            </label>
          </div>


        </div>    
      </div>
      <div class="row">
        <div class="col-sm-12">
          <fieldset class="form-group">  
            <label for="owner_email">E-mail</label>
            <input type="text" class="form-control" name="owner_email" value="<?php echo $ownerEmail ?>">
          </fieldset>
        </div>
      </div>    
      <div class="row">
        <div class="col-sm-6">
          <fieldset class="form-group">  
            <label for="owner_fname">First Name</label>
            <input type="text" class="form-control" name="owner_fname" value="<?php echo $ownerFname ?>">
          </fieldset>
        </div>
        <div class="col-sm-6">  
          <fieldset class="form-group">  
            <label for="owner_lname">Last Name</label>
            <input type="text" class="form-control" name="owner_lname" value="<?php echo $ownerLname ?>">
          </fieldset>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">      
          <fieldset class="form-group">  
            <label for="owner_role">Role</label>
            <input type="text" class="form-control" name="owner_role" value="<?php echo $ownerRole ?>">
          </fieldset>
        </div>
        <div class="col-sm-6">      
          <fieldset class="form-group">  
            <label for="owner_medctr">Med Ctr</label>
            <input type="text" class="form-control" name="owner_medctr" value="<?php echo $ownerMedctr ?>">
          </fieldset>
        </div>
      </div>    
      <div class="row">
        <div class="col-sm-6">      
          <fieldset class="form-group">  
            <label for="owner_dept">Department</label>
            <input type="text" class="form-control" name="owner_dept" value="<?php echo $ownerDept ?>">
          </fieldset>
        </div>
        <div class="col-sm-6">      
          <fieldset class="form-group">  
            <label for="owner_location">Location</label>
            <input type="text" class="form-control" name="owner_location" value="<?php echo $ownerLocation ?>">
          </fieldset>
        </div>
      </div>  


<script>
jQuery(document).ready( function($) {
	$("#selExceptionId, #selApplicationId").chosen();
});
</script> 
