<?php
require_once('inc_db.php');
global $mysqli;

$detailsErrors         = array();      // array to hold validation errors
$detailsData           = array();      // array to pass back data

// validate the variables ======================================================
    // if any of these variables don't exist, add an error to our $errors array

    if (empty($_POST['device_type_id'])) {
        $detailsErrors['device_type_id'] = 'Device Type is required.';
		}

// return a response ===========================================================

    // if there are any errors in our errors array, return a success boolean of false
    if ( ! empty($detailsErrors)) {

        // if there are items in our errors array, return those errors
        $detailsData['detailsSuccess'] = false;
        $detailsData['detailsErrors']  = $detailsErrors;
    } else {

				// if there are no errors process our form, then return a message
				$device_type_id = mysqli_real_escape_string($mysqli, $_POST['device_type_id']);
				$device_model = mysqli_real_escape_string($mysqli, $_POST['device_model']);
				$device_carrier = mysqli_real_escape_string($mysqli, $_POST['device_carrier']);
				$device_identifier1 = mysqli_real_escape_string($mysqli, $_POST['device_identifier1']);
				$device_identifier2 = mysqli_real_escape_string($mysqli, $_POST['device_identifier2']);
				$device_project = mysqli_real_escape_string($mysqli, $_POST['device_project']);
				$device_status_id = (!empty($_POST['device_status_id']) ? mysqli_real_escape_string($mysqli, $_POST['device_status_id']) : 'null');  
				$owner_nuid = mysqli_real_escape_string($mysqli, $_POST['owner_nuid']);
				$owner_fname = mysqli_real_escape_string($mysqli, $_POST['owner_fname']);
				$owner_lname = mysqli_real_escape_string($mysqli, $_POST['owner_lname']);
				$owner_email = mysqli_real_escape_string($mysqli, $_POST['owner_email']);
				$owner_role = mysqli_real_escape_string($mysqli, $_POST['owner_role']);
				$owner_medctr = mysqli_real_escape_string($mysqli, $_POST['owner_medctr']);
				$owner_location = mysqli_real_escape_string($mysqli, $_POST['owner_location']);
				$owner_dept = mysqli_real_escape_string($mysqli, $_POST['owner_dept']);
				$attestation_on_file = (!empty($_POST['attestation_on_file']) ? mysqli_real_escape_string($mysqli, $_POST['attestation_on_file']) : 'null');
				
				// Process the Device/Owner Details
				$detailsSql = "
					UPDATE device
					SET
						device_type_id = " . $device_type_id . ",
						device_model = '" . $device_model . "',
						device_carrier = '" . $device_carrier . "',
						device_identifier1 = '" . $device_identifier1 . "',
						device_identifier2 = '" . $device_identifier2 . "',
						device_project = '" . $device_project . "',
						device_status_id = " . $device_status_id . ",
						owner_nuid = '" . $owner_nuid . "',
						owner_fname = '" . $owner_fname . "',
						owner_lname = '" . $owner_lname . "',
						owner_email = '" . $owner_email . "',
						owner_role = '" . $owner_role . "',
						owner_medctr = '" . $owner_medctr . "',
						owner_location = '" . $owner_location . "',
						owner_dept = '" . $owner_dept . "',
						attestation_on_file = " . $attestation_on_file . "
					WHERE device_id = " . $_POST['device_id'];
				if ($mysqli->query($detailsSql) === TRUE) {
					$detailsData['detailsSuccess'] = true;
					$detailsData['detailsMessage'] = 'Success!';
				} else {					// show a message of failure and provide a variable
					$detailsData['detailsSuccess'] = false;
					$detailsData['detailsMessage'] = 'Fail! ' . $detailsSql;
				}
				
				// Process the Mobile Apps
				$deleteApplicationSql = "DELETE FROM device_application WHERE device_id = " . $_POST['device_id'];
				$mysqli->query($deleteApplicationSql);
				
				if (!empty($_POST['application_id'])) {
					foreach($_POST['application_id'] as $applicationId) {
						$addApplicationSql = "INSERT INTO device_application(device_id, application_id) VALUES (" . $_POST['device_id'] . "," . $applicationId . ")";
						$mysqli->query($addApplicationSql);
					}
				}

				// Process the Exceptions (Issues)
				$deleteExceptionSql = "DELETE FROM device_exception WHERE device_id = " . $_POST['device_id'];
				$mysqli->query($deleteExceptionSql);
				
				if (!empty($_POST['exception_id'])) {
					foreach($_POST['exception_id'] as $exceptionId) {
						$addExceptionSql = "INSERT INTO device_exception(device_id, exception_id) VALUES (" . $_POST['device_id'] . "," . $exceptionId . ")";
						$mysqli->query($addExceptionSql);
					}
				}

    }

    // return all our data to an AJAX call
    echo json_encode($detailsData);


