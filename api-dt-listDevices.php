<?php
require('inc_db.php');


	global $mysqli;
	
	// $sortBy = !empty($_REQUEST['jtSorting']) ? $_REQUEST['jtSorting'] : "device.device_id ASC";

	// LEFT JOIN (SELECT device_id, CONCAT('[', date,']: ',content) as combined FROM note LIMIT 1) the_alias ON device.device_id = the_alias.device_id	
	/* Original Query:
			SELECT d1.device_id, device_type.name AS device_type_name, d1.device_model, d1.device_identifier1, d1.device_identifier2, d1.device_carrier, device_status.name AS device_status_name, device_project, GROUP_CONCAT(device_exception.exception_id SEPARATOR '|') as exception_id, d1.owner_nuid, d1.owner_fname, d1.owner_lname, d1.owner_email, d1.owner_medctr, d1.owner_location, d1.owner_role, d1.owner_email, d1.owner_dept, n1.content
			FROM device as d1
			LEFT JOIN note as n1 ON d1.device_id = n1.device_id
				AND n1.note_id = (SELECT MAX(n2.note_id) FROM note as n2 WHERE n2.device_id = d1.device_id)
			
			LEFT JOIN device_exception ON d1.device_id = device_exception.device_id
				
			LEFT JOIN device_type ON d1.device_type_id = device_type.device_type_id 
			LEFT JOIN device_status ON d1.device_status_id = device_status.device_status_id 
			GROUP BY d1.device_id 
			ORDER BY d1.device_id
	*/	

	$sql = "
			SELECT d1.device_id, device_type.name AS device_type_name, d1.device_model, d1.device_identifier1, d1.device_identifier2, d1.device_carrier, device_status.name AS device_status_name, device_project, GROUP_CONCAT(DISTINCT device_exception.exception_id SEPARATOR ' | ') as exception_id_list, GROUP_CONCAT(DISTINCT exception.name SEPARATOR ' | ') as exception_name_list, d1.owner_nuid, d1.owner_fname, d1.owner_lname, d1.owner_email, d1.owner_medctr, d1.owner_location, d1.owner_role, d1.owner_email, d1.owner_dept, GROUP_CONCAT(DISTINCT note.content ORDER BY date DESC SEPARATOR ' | ') as content
			FROM device as d1
			LEFT JOIN note ON d1.device_id = note.device_id
			LEFT JOIN device_exception ON d1.device_id = device_exception.device_id
			LEFT JOIN exception ON device_exception.exception_id = exception.exception_id
			LEFT JOIN device_type ON d1.device_type_id = device_type.device_type_id 
			LEFT JOIN device_status ON d1.device_status_id = device_status.device_status_id
			WHERE (true) ";
	if (isset($_REQUEST['exceptionId']) && !empty($_REQUEST['exceptionId'])) {
		$sql .= "AND device_exception.exception_id = " . $_REQUEST['exceptionId'] . " ";
	}
	if (isset($_REQUEST['devicetypeId']) && !empty($_REQUEST['devicetypeId'])) {
		$sql .= "AND device_type.device_type_id = " . $_REQUEST['devicetypeId'] . " ";
	}
	$sql .= "						
			GROUP BY d1.device_id 
			ORDER BY d1.device_id
			";
			//echo $sql;

	if ($result = $mysqli->query($sql)) {
		$rows = array();
		while($r = mysqli_fetch_assoc($result)) {
		    $rows[] = $r;
		}
		
		$countResult = $mysqli->query("SELECT COUNT(*) FROM device");
		$countRow = $countResult->fetch_row();
		$count = $countRow[0];

		$data = array();
		$data['data'] = $rows;
		//$data['TotalRecordCount'] = $count;

		print json_encode($data);
		//$result->close();
	}
