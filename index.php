<?php
require('inc_db.php');
global $mysqli;

require '../../kpnvly/wp-load.php';
?>
<!DOCTYPE html>
<html>
<head>

<!-- Web Fonts -->
<!--
<link rel='stylesheet' type='text/css' href="./css/font-roboto.css">
<link rel="stylesheet" type='text/css' href="./css/font-material-icons.css">
-->

<!-- jQuery 1.x -->
<!-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.1.min.js"></script> -->

<!-- Datatables -->
<!-- <link rel="stylesheet" type="text/css" href="./js/DataTables/DataTables-1.10.11/css/dataTables.jqueryui.min.css"/> -->
<!-- <link rel="stylesheet" type="text/css" href="./js/DataTables/Buttons-1.1.2/css/buttons.jqueryui.min.css"/> -->
<!-- DO NOT USE <link rel="stylesheet" type="text/css" href="./js/DataTables/Responsive-2.0.2/css/responsive.jqueryui.min.css"/> -->
<!--
<script type="text/javascript" src="./js/DataTables/JSZip-2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="./js/DataTables/pdfmake-0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" src="./js/DataTables/pdfmake-0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" src="./js/DataTables/DataTables-1.10.11/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="./js/DataTables/DataTables-1.10.11/js/dataTables.jqueryui.min.js"></script>
<script type="text/javascript" src="./js/DataTables/Buttons-1.1.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="./js/DataTables/Buttons-1.1.2/js/buttons.jqueryui.min.js"></script>
<script type="text/javascript" src="./js/DataTables/Buttons-1.1.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="./js/DataTables/Buttons-1.1.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="./js/DataTables/Buttons-1.1.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="./js/DataTables/Buttons-1.1.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="./js/DataTables/Responsive-2.0.2/js/dataTables.responsive.min.js"></script>
-->

<!-- DO NOT USE <script type="text/javascript" src="./js/DataTables/Responsive-2.0.2/js/responsive.jqueryui.min.js"></script> -->

<!-- Bootstrap  -->
<!--
<script src="./js/bootstrap.min.js"></script>
<link rel="stylesheet" href="./css/bootstrap.min.css">
-->

<!-- Custom jQuery UI Theme -->
<!--
<script src="./js/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<link rel="stylesheet" href="./js/jquery-ui-1.11.4.custom/jquery-ui.css">
-->

<!-- Chosen Dropdowns -->

<style>
html, body {
	font-family: 'Roboto', sans-serif!important;
	font-size: 10px!important;
}
h4 {
	font-style: normal!important;
}
#Content {
	padding-top: 30px!important;
	max-width: 100%!important;
	min-width: 100%!important;
}
select.form-control, input.form-control {
	height: 28px!important;
	padding: 2px 8px!important;
}
.appTitle {
	font-size: 18px!important;
	margin-left: 20px;
}
.form-group {
	margin-bottom: 10px!important;
}
.ui-widget {
	font-family: 'Roboto', sans-serif!important;
	font-size: 12px!important;
}
#deviceTable_wrapper input, #deviceTable_wrapper select {
	font-family: 'Roboto', sans-serif!important;
	font-size: 12px!important;
	color: #666!important;
	height: 30px!important;
}
#deviceTable_wrapper {
	background-color: #337ab7!important;
	padding: 5px!important;
}
#deviceTable_wrapper label, #deviceTable_info {
	color: #ffffff!important;
}
.dataTables_scroll {
	padding-top: 5px!important;
}
#flagFilter, #devicetypeFilter {
	display: inline-block!important;
	margin-left: 10px!important;
}
.toolbar {
	display: block!important;
	float: left!important;
}
.dataTables_length, .dataTables_filter {
	margin-top: 0px!important;
}
.filterContainer {
	display: inline!important;
	float: right!important;
}
.noteDate {
	font-weight: bold!important;
}
.no-notes {
	text-align: center!important;
	padding: 10px!important;
}
.dt-cell-nowrap {
	white-space: nowrap!important;
}
.dt-cell-center {
	text-align: center!important;
}
.dt-cell-right {
	text-align: right!important;
}
.table-responsive {
	overflow-x: inherit!important;
}
/* Rules for sizing the icon. */
.material-icons.md-18 {
	font-size: 18px!important;
}
.material-icons.md-24 {
	font-size: 24px!important;
}
.material-icons.md-36 {
	font-size: 36px!important;
}
.material-icons.md-48 {
	font-size: 48px!important;
}
.ui-state-disabled, .ui-widget-content .ui-state-disabled, .ui-widget-header .ui-state-disabled {
	opacity: 0.7!important;
}
.dt-buttons {
	display: inline!important;
}
div.dt-buttons .dt-button {
	margin-right: 5px!important;
}
.dataTables_length, .dataTables_filter {
	margin-top: 5px!important;
}
.dataTables_wrapper .dataTables_paginate .ellipsis {
	color: #fff!important;
}
</style>
</head>
<?php require_once 'inc/header.php'; ?>

<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="pull-left"><img src="./images/logo_oim.png" height="30" align="absmiddle" vspace="10" hspace="10"><span class="appTitle">Device Inventory Asset Manager</span></div>
      <div class="pull-right" style="margin: 10px!important">
        <button onclick="showAddDevice()" class="btn btn-primary">Add New Device</button>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 table-responsive">
      <table class="table cell-border compact hover" id="deviceTable">
        <thead>
          <tr>
            <th nowrap style='white-space: nowrap'></th>
            <th nowrap style='white-space: nowrap'>Device Type</th>
            <th nowrap style='white-space: nowrap'>Device Model</th>
            <th nowrap style='white-space: nowrap'>Carrier</th>
            <th nowrap style='white-space: nowrap'>Identifier 1</th>
            <th nowrap style='white-space: nowrap'>Identifier 2</th>
            <th nowrap style='white-space: nowrap'>Project</th>
            <th nowrap style='white-space: nowrap'>Status</th>
            <th nowrap style='white-space: nowrap'>Flags</th>
            <th nowrap style='white-space: nowrap'>Flags</th>
            <th nowrap style='white-space: nowrap'>NUID</th>
            <th nowrap style='white-space: nowrap'>First Name</th>
            <th nowrap style='white-space: nowrap'>Last Name</th>
            <th nowrap style='white-space: nowrap'>Email</th>
            <th nowrap style='white-space: nowrap'>Role</th>
            <th nowrap style='white-space: nowrap'>Med Ctr</th>
            <th nowrap style='white-space: nowrap'>Department</th>
            <th nowrap style='white-space: nowrap'>Location</th>
            <th nowrap style='white-space: nowrap'>Notes</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</div>
<br clear="all">
<div id="flagFilter">
  <label>Flags:</label>
  <select id="flagFilterInput" onchange="filterResults()">
    <option value="">All</option>
    <?php
$xSql = "SELECT * FROM exception ORDER BY name ASC";
if ($xResult = $mysqli->query($xSql)) {
while($xRow = mysqli_fetch_assoc($xResult)) {
?>
    <option value="<?php echo $xRow['exception_id'] ?>"><?php echo $xRow['name'] ?></option>
    <?php
}
}
?>
  </select>
</div>
<div id="devicetypeFilter">
  <label>Device Type:</label>
  <select id="devicetypeFilterInput" onchange="filterResults()">
    <option value="">All</option>
    <?php
$dtSql = "SELECT * FROM device_type ORDER BY name ASC";
if ($dtResult = $mysqli->query($dtSql)) {
while($dtRow = mysqli_fetch_assoc($dtResult)) {
?>
    <option value="<?php echo $dtRow['device_type_id'] ?>"><?php echo $dtRow['name'] ?></option>
    <?php
}
}
?>
  </select>
</div>

<!-- Dialog container for the modal popups -->
<div id="dialog"></div>
<script>
// Global variables that will be used for the helper functions 
var showAddDevice;
var showEditDevice;
var showEditNotes;
var filterResults;
var deviceTable;

jQuery(document).ready( function($) {
  // Force non-caching for AJAX calls. More important for IE/Edge
  $.ajaxSetup({
	  cache: false
  });

  // Show the Add Device dialog
  showAddDevice = function(deviceId) {
	  $("#dialog").load('addDevice.php?r=' + Math.random());
	  $("#dialog").dialog( "option", "width", 1024 );
	  $("#dialog").dialog( "option", "height", 818 );
	  $("#dialog").dialog( "option", "title", "Add Device");
	  $('#dialog').dialog({
		show: { effect: "fade", duration: 300 },
		  hide: { effect: "fade", duration: 300 },
	  });
	  $('#dialog').dialog('open');
  }
  
  // Shhow the Edit Device dialog
  showEditDevice = function(deviceId, fieldId) {
	  $("#dialog").load('editDevice.php?deviceId=' + deviceId + '&r=' + Math.random() + '&f=' + fieldId);
	  $("#dialog").dialog( "option", "width", 1024 );
	  $("#dialog").dialog( "option", "height", 818 );
	  $("#dialog").dialog( "option", "title", "Edit Device");
	  $('#dialog').dialog({
		show: { effect: "fade", duration: 300 },
		  hide: { effect: "fade", duration: 300 },
	  });
	  $('#dialog').dialog('open');
  }
  
  // Show the Edit Notes dialog
  showEditNotes = function(deviceId) {
	  $("#dialog").load('editDeviceNotes.php?deviceId=' + deviceId + '&r=' + Math.random());
	  $("#dialog").dialog( "option", "width", 512 );
	  $("#dialog").dialog( "option", "height", 768 );
	  $("#dialog").dialog( "option", "title", "Edit Notes");
	  $('#dialog').dialog({
		show: { effect: "fade", duration: 300 },
		  hide: { effect: "fade", duration: 300 }
	  });
	  $('#dialog').dialog('open');
  }
  
  filterResults = function() {
	  selFlag = $('#flagFilterInput').val();
	  selDeviceType = $('#devicetypeFilterInput').val();
	  deviceTable.ajax.url('api-dt-listDevices.php?exceptionId=' + selFlag + '&devicetypeId=' + selDeviceType).load(null, false);	
  }

  // Set up the Datatable
  deviceTable = $('#deviceTable').DataTable( {
	  responsive: false,
	  "pageLength": 25,
	  "processing": true,
	  dom: '<"toolbar"Bl<"filterContainer">>frtip',
	  buttons: [
        {
            extend: 'copy',
            text: 'Copy',
            exportOptions: {
				columns: [1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,18]
            }
        },
        {
            extend: 'excel',
            text: 'Excel',
            exportOptions: {
				columns: [1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,18]
            }
        }
	  ],
	  scrollX: true,
	  ajax: {
		  "dataType": "json",
		  "url": "api-dt-listDevices.php",
		  "type": "POST",
		  "cache": false
	  },
	  "initComplete": function(settings, json) {
		$('#deviceTable_filter').append($('#devicetypeFilter'));
		$('#deviceTable_filter').append($('#flagFilter'));
	  },
	  "columns": [
				  {
					  "data": "device_id",
					  "title": "Edit",
					  "orderable" : false,
					  "searchable" : false,
					  "render" : function(data, type, row) {
						  if (data) {
							  return "<div style='text-align: center'><a href='#' class='editIconLink' title='Edit' style='cursor: pointer' onclick=\"showEditDevice(" + data + ")\"><img src='./images/ic_create_black_18dp_1x.png'></a></div>";
						  } else {
							  return null;
						  }
					  },
				  },
				  { "data": "device_type_name" },
				  { "data": "device_model" },
				  { "data": "device_carrier" },
				  {
					  "data": "device_identifier1",
					  "render": function(data, type, row) {
						  return "<span style='white-space:nowrap'>" + data + "</span>";	
					  },
				  },
				  {
					  "data": "device_identifier2",
					  "className" : "dt-cell-nowrap"
				  },
				  { "data": "device_project" },
				  {
					  "data": "device_status_name",
					  "className" : "dt-cell-nowrap",
				  },
				  {
					  "data": "exception_id_list",
					  "searchable" : true,
					  "className" : "dt-cell-center dt-cell-nowrap",
					  "render": function(data, type, row) {
						  if (data) {
							  var flagList = data.split(" | ");
							  var flagStr = "";
							  for (i=0; i < flagList.length; i++) {
								flagStr += "<img src='./images/icon_flag_" + flagList[i] + ".png' width='17' height='17' hspace='2'>";
							  }
							  return flagStr;
						  } else {
							  return "";
						  }
					  },
				  },
				  {
					  "data": "exception_name_list",
					  "searchable" : true,
					  "visible" : false,
					  "render": function(data, type, row) {
						  if (data) {
							  return data;
						  } else {
							  return "";
						  }
					  },
				  },
				  { "data": "owner_nuid" },
				  { "data": "owner_fname" },
				  { "data": "owner_lname" },
				  {
							  "data": "owner_email",
							  "render": function(data, type, row) {
								  if (data) {
									  return "<a href='mailto:" + data.toLowerCase() + "'>" + data.toLowerCase() + "</a>";	
								  } else {
									  return null;
								  }
							  },
						  },
				  { "data": "owner_role" }, 
				  { "data": "owner_medctr" },
				  {
					  "data": "owner_dept",
					  "className" : "dt-cell-nowrap",
				  },
				  {
					  "data": "owner_location",
					  "render": function(data, type, row) {
						  if (data) {
							  return "<span style='white-space:nowrap'>" + data + "</span>";	
						  } else {
							  return null;
						  }
					  },
				  },
				  {
					  "data": "content",
					  "orderable" : true,
					  "searchable" : true,
					  "render" : function(data, type, row) {
						  if (data) {
							  dataArr = data.split(' | ');
							  return "<div style='text-align: center'><a href='#' class='noteIconLink' title='" + dataArr[0] + "' style='cursor: pointer' onclick=\"showEditNotes(" + row.device_id + ")\"><img src='./images/ic_insert_comment_black_18dp_1x.png'></a></div><div style='display: none'>" + data + "</div>";
						  } else {
							  return null;
						  }
					  },
				  }
				  
	  ],
  } );
  
  //deviceTable.buttins().container().appendTo('#bContainer');

  // Listen for double-clicks on table rows to generate the Edit Device popup
  $("#deviceTable tbody").on("dblclick", "td", function() {
	  //var rowData = deviceTable.row(this).data();
	  var theCell = deviceTable.cell(this);
	  var theRowIdx = theCell.index().row;
	  var theColumnIdx = theCell.index().column;
	  var theRow = deviceTable.row(theRowIdx);
	  var theColumn = deviceTable.column(theColumnIdx);
	  var deviceId = theRow.data().device_id;
	  var fieldId = theColumn.dataSrc();
	  //alert('You clicked on Device ' + deviceId + ', Field ' + columnId);
	  showEditDevice(deviceId, fieldId);
  });

	$('div.dt-buttons .dt-button').addClass('btn btn-primary');

  // Configure the jQuery dialog used for Edit/Add Device operations
  $("#dialog" ).dialog({ autoOpen: false, modal: true, });

// });

});

</script>
<?php include_once "inc/footer.php"; ?>
