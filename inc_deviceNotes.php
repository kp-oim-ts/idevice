<?php
/*
	inc_deviceNotes.php
*/
?>
    	<h4>Notes</h4>
    	<div class="row">
      	<div class="col-sm-12">
          <fieldset class="form-group">  
            <label for="new_note">Add a New Note</label>
            <textarea class="form-control" name="new_note"></textarea>
          </fieldset>
          <div id="notesContainer" style="border: 1px solid #ccc; background-color: #f6f6f6; height: 500px; overflow-y: scroll;">
<?php
	if (isset($deviceId)) {	
		$noteSql = "SELECT * FROM note WHERE device_id = " . $deviceId . " ORDER BY date DESC";
		if ($noteResult = $mysqli->query($noteSql)) {
			if ($noteResult->num_rows > 0) {
				while ($noteRow = $noteResult->fetch_assoc()) {
?>
						<div style="background-color: #ffffff; border: 1px solid #eee; padding: 5px; margin: 5px;">
            	<div class="noteDate"><?php echo $noteRow['date'] ?></div>
	            <?php echo isset($noteRow['content']) ? $noteRow['content'] : "" ?>
            </div>
<?php
				}
			} else {
				echo "<div class='no-notes'>There are currently no notes to display.</div>";
			}
		} else {
			echo "<div class='no-notes'>There are currently no notes to display.</div>";
		}
	} else {
		echo "<div class='no-notes'>There are currently no notes to display.</div>";
	}
?>		          
          </div>
        </div>
      </div>  
