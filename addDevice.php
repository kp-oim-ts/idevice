<?php
require('inc_db.php');
global $mysqli;
?>

<div class="container-fluid">
  <form id="addForm" method="post">
    <div class="row">
      <div class="col-sm-6" style="border-right: 1px dotted #ccc">
        <?php
include 'inc_deviceDetails.php';
?>
      </div>
      <div class="col-sm-6">
        <?php 
include 'inc_deviceNotes.php';
?>
      </div>
    </div>
    <div class="row" style="border-top: 1px dotted #ccc; padding-top: 20px">
      <div class="col-sm-12">
        <div class="pull-right">
          <button type="button" class="btn btn-primary-outline" onClick="jQuery('#dialog').dialog('close');">Cancel</button>
          &nbsp;
          <input class="btn btn-primary" type="submit" value="Add Device">
        </div>
      </div>
    </div>
  </form>
  <script>
// Attach a submit handler to the form

jQuery(document).ready( function($) {

  $("#addForm").on("submit", function( event ) {
	  
	  notesSuccess = true;
	  detailsSuccess = true;
	  
	  // stop the form from submitting the normal way and refreshing the page
	  event.preventDefault();
  
	  // get the form data
	  // there are many ways to get this data using jQuery (you can use the class or id also)
	  var formData = $("#addForm").serialize();
  
	  // process the form for details
	  $.ajax({
			  type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
			  url         : 'processAddDevice.php', // the url where we want to POST
			  data        : formData, // our data object
			  dataType    : 'json', // what type of data do we expect back from the server
			  encode      : true
	  })
			  // using the done promise callback
			  .done(function(detailsData) {
	  
					  // log data to the console so we can see
					  // console.log(detailsData); 
  
					  if (detailsData['detailsSuccess'] === false) {
						  detailsSuccess = false;
					  }
  
					  if (detailsData['detailsSuccess'] === true) {
						  // Reload the parent table
						  deviceTable.ajax.reload(null, false);
						  
						  // Close the dialog
						  $("#dialog" ).dialog("close");
					  }
  
			  });
  
  
  
  
  
	  
  });

});

</script> 
</div>
