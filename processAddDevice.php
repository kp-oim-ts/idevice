<?php
require_once('inc_db.php');
global $mysqli;

$newDeviceId =  null;

$detailsErrors         = array();      // array to hold validation errors
$detailsData           = array();      // array to pass back data

// validate the variables ======================================================
    // if any of these variables don't exist, add an error to our $errors array

    if (empty($_POST['device_type_id'])) {
        $detailsErrors['device_type_id'] = 'Device Type is required.';
		}

// return a response ===========================================================

    // if there are any errors in our errors array, return a success boolean of false
    if ( ! empty($detailsErrors)) {

        // if there are items in our errors array, return those errors
        $data['detailsSuccess'] = false;
        $data['detailsErrors']  = $detailsErrors;
    } else {

        // if there are no errors process our form, then return a message

        // DO ALL YOUR FORM PROCESSING HERE
        // THIS CAN BE WHATEVER YOU WANT TO DO (LOGIN, SAVE, UPDATE, WHATEVER)
				$detailsSql = "
					INSERT INTO device(device_type_id, device_model, device_carrier, device_identifier1, device_identifier2, device_project, device_status_id, owner_nuid, owner_fname, owner_lname, owner_email, owner_role, owner_medctr, owner_location, owner_dept, attestation_on_file)
					VALUES(" .
						$_POST['device_type_id'] . ", " .
						"'" . $_POST['device_model'] . "', " . 
						"'" . $_POST['device_carrier'] . "', " .
						"'" . $_POST['device_identifier1'] . "', " .
						"'" . $_POST['device_identifier2'] . "', " .
						"'" . $_POST['device_project'] . "', " .
						(!empty($_POST['device_status_id']) ? $_POST['device_status_id'] : 'null') . ", " .
						"'" . $_POST['owner_nuid'] . "', " .
						"'" . $_POST['owner_fname'] . "', " .
						"'" . $_POST['owner_lname'] . "', " .
						"'" . $_POST['owner_email'] . "', " .
						"'" . $_POST['owner_role'] . "', " .
						"'" . $_POST['owner_medctr'] . "', " .
						"'" . $_POST['owner_location'] . "', " .
						"'" . $_POST['owner_dept'] . "', " .
						(!empty($_POST['attestation_on_file']) ? $_POST['attestation_on_file'] : 'null') .
						")";
				if ($mysqli->query($detailsSql) === TRUE) {
					$data['detailsSuccess'] = true;
					$data['detailsMessage'] = 'Success!';
				} else {					// show a message of failure and provide a variable
					$data['detailsSuccess'] = false;
					$data['detailsMessage'] = 'Fail! ' . $detailsSql;
				}
				$newDeviceId =  $mysqli->insert_id;
    }

		// Process the Mobile Apps
		$deleteApplicationSql = "DELETE FROM device_application WHERE device_id = " . $newDeviceId;
		$mysqli->query($deleteApplicationSql);
		
		if (!empty($_POST['application_id'])) {
			foreach($_POST['application_id'] as $applicationId) {
				$addApplicationSql = "INSERT INTO device_application(device_id, application_id) VALUES (" . $newDeviceId . "," . $applicationId . ")";
				$mysqli->query($addApplicationSql);
			}
		}

		// Process the Exceptions (Issues)
		$deleteExceptionSql = "DELETE FROM device_exception WHERE device_id = " . $newDeviceId;
		$mysqli->query($deleteExceptionSql);
		
		if (!empty($_POST['exception_id'])) {
			foreach($_POST['exception_id'] as $exceptionId) {
				$addExceptionSql = "INSERT INTO device_exception(device_id, exception_id) VALUES (" . $newDeviceId . "," . $exceptionId . ")";
				$mysqli->query($addExceptionSql);
			}
		}


		if (!empty($_POST['new_note'])) {
			$notesSql = "
				INSERT INTO note(date, device_id, content)
				VALUES(
					'" . date("Y-m-d G:i:s") . "', 
					" . $newDeviceId . ",
					'" . $_POST['new_note'] . "')";
			if ($mysqli->query($notesSql) === TRUE) {
				$data['notesSuccess'] = true;
				$data['notesMessage'] = 'Success!';
			} else {					// show a message of failure and provide a variable
				$data['notesSuccess'] = false;
				$data['notesMessage'] = 'Fail! ' . $notesSql;
			}
		}


    // return all our data to an AJAX call
    echo json_encode($data);


