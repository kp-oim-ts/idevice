<?php
require_once('inc_db.php');
global $mysqli;

$notesErrors         = array();      // array to hold validation errors
$notesData           = array();      // array to pass back data

// validate the variables ======================================================
    // if any of these variables don't exist, add an error to our $errors array

		// (Since there is only one form field, and we check it for empty() below,
		// there really is no error condition here.)

// return a response ===========================================================

    // if there are any errors in our errors array, return a success boolean of false
    if ( ! empty($notesErrors)) {

        // if there are items in our errors array, return those errors
        $notesData['notesSuccess'] = false;
        $notesData['notesErrors']  = $notesErrors;
    } else {

        // if there are no errors process our form, then return a message
			if (!empty($_POST['new_note'])) {
				$new_note = mysqli_real_escape_string($mysqli, $_POST['new_note']);
				
				$notesSql = "
					INSERT INTO note(date, device_id, content)
					VALUES(
						'" . date("Y-m-d G:i:s") . "', 
						" . $_POST['device_id'] . ",
						'" . $new_note . "')";
				if ($mysqli->query($notesSql) === TRUE) {
					$notesData['notesSuccess'] = true;
					$notesData['notesMessage'] = 'Success!';
				} else {					// show a message of failure and provide a variable
					$notesData['notesSuccess'] = false;
					$notesData['notesMessage'] = 'Fail! ' . $notesSql;
				} // if
			} // if
    } // else

    // return all our data to an AJAX call
    echo json_encode($notesData);


