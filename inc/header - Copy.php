<?php

date_default_timezone_set(get_option('timezone_string'));

// remove more_announcements. We don't want them in this application
function giw_dequeue_script() {
	wp_dequeue_script( 'more_announcements' );
	wp_dequeue_script( 'kpit_alerts_ajax' );
	wp_dequeue_script( 'show_homepage_slides' );
}
add_action( 'wp_print_scripts', 'giw_dequeue_script', 100 );

//Making jQuery Google API
function giw_modify_jquery() {
	if (!is_admin()) {
		// comment out the next two lines to load the local copy of jQuery
		wp_deregister_script('jquery');
		wp_register_script('jquery', 'http://'.$_SERVER['HTTP_HOST'].'/utility/gi-whiteboard/js/jquery.min.js', array(), '1.11.0',true);
		wp_enqueue_script('jquery');
	}
}
add_action('init', 'giw_modify_jquery');

// enqueue all javascripts universal to this application
function giw_enqueue_everything(){
	$server = $_SERVER['HTTP_HOST'];
	
	//**		enqueued stylesheets always appear in the page header     **//
	wp_enqueue_style('jquery-ui','http://'.$server.'/utility/gi-whiteboard/css/jquery-ui.min.css');	
	wp_enqueue_style('dataTables','http://'.$server.'/utility/gi-whiteboard/css/jquery.dataTables.min.css',array('jquery-ui'));
	wp_enqueue_style('primary_styles','http://'.$_SERVER['SERVER_NAME'].'/utility/gi-whiteboard/css/style.css', array('jquery-ui'));	
//	wp_enqueue_style('dataTables-resp','//cdn.datatables.net/responsive/1.0.2/css/dataTables.responsive.css');
	
	
	//**		enqueue scripts to appear in the page header     **//
	

	//**		enqueue scripts to appear in the page footer     **//

	// for use with dialog and other jQuery UI widgets
	wp_enqueue_script('jquery');
	
	wp_enqueue_script( 'jquery-ui','http://'.$server.'/utility/gi-whiteboard/js/jquery-ui.js',array('jquery'),'',true);
	wp_enqueue_script( 'dataTables','http://'.$server.'/utility/gi-whiteboard/js/jquery.dataTables.min.js',array('jquery-ui'),'',true);

	// jquery cookie support
	wp_enqueue_script('jquery-cookie','http://'.$_SERVER['SERVER_NAME'].'/wp-content/themes/kaiser/assets/js/jquery.cookie.js',array('jquery'),'2.2.0',false);
	// user login support
	wp_enqueue_script( 'logging','http://'.$server.'/utility/gi-whiteboard/js/user_login.js',array('jquery'),'.5',true);

	
	//  NVLY field validation
	wp_enqueue_script( 'common_utils','http://'.($server=='localhost'?'cnrpwwebd001.rpw.ca.kp.org':$server).'/scripts/common_utils.min.js',array(),'1.5',true);
	
	$js_root = 'http://'.$_SERVER['SERVER_NAME'].'/utility/gi-whiteboard/js/';
	$parent_path = dirname($_SERVER["SCRIPT_FILENAME"]);
	$separator = stristr($parent_path,'/') === false ? '\\' : '/';
	$path_parts = explode($separator, $parent_path);
	$filename = array_pop($path_parts);
	switch ($filename){
		case 'users':
			$js_file = 'giw_user_functions.js';
			break;
		case 'groups':
			$js_file = 'giw_groups_functions.js';
			break;
		default:
			$js_file = 'giw_functions.js';
			break;
	}
	wp_enqueue_script('local',$js_root.$js_file,array('jquery'),'2.2.0',true);

	
} // end function enqueue_everything
add_action('wp_enqueue_scripts', 'giw_enqueue_everything');

add_filter('body_class', 'giwb_body_classes');

function giwb_body_classes($classes) {
	if ( isset($classes['error404']) ) unset($classes['error404']);
    if ( isset($_SESSION['user']) ) {
		$classes[] = 'logged-in';
		$classes[] = 'single-author';
	}
	$classes[] = 'page';
	$classes[] = 'singular';
	$classes[] = 'giwb';
    return $classes;
}

function getMySQL_Today(){
	$phptime = strtotime('today midnight');
	return date ("Y-m-d H:i:s", $phptime);
}

get_header();
?>