<?php
/*************************************
/*	class.sqlidb.php
/*	PURPOSE:  Wrapper and checker for 
/*		MySQL connection to database via MySQLi.
/*		Default and global variables can be used
/*		internally, or via Global reference.
/*	AUTHOR:		Michael Baxter
/*				michael@kp.org
/*	ORGANIZATION:
/*		Operations Information Management
/*		North Sacramento Valley TPMG
/****************************************/
#Section 1.1: Connection data
$_sqli_conf['DB_SERVER']	= 'cneurwebp001.eur.ca.kp.org';         //MySql server
$_sqli_conf['DB_DATABASE']	= 'gi-whiteboard';                     //DB Name
$_sqli_conf['DB_PASSWORD']	= 'giwb@dmin!';                         //MySql password
$_sqli_conf['DB_USERNAME']	=  'giwb_admin';
$_sqli_conf['PORT'] = '3306';

if (!class_exists('MySQLi_DB')){

	class MySQLi_DB{

		private $db;
		
		private $is_valid;
		
		private $dbName;
		
		private $dbPwd;
		
		private $dbServer;
		
		private $dbUser;
		
		private $dbPort;
		
		private $connect_error;
		
		private $connect_errno;
		
		public $row_count;
		
		public $error;
		
		public $error_num;
		
		public $last_query;
		
		public $insert_id;
	
				
		function __construct($_PARAMS=null){
			global $_sqli_conf;
			
			if ($_PARAMS == null){
				$this->dbServer = $_sqli_conf['DB_SERVER'];
				$this->dbName = $_sqli_conf['DB_DATABASE'];
				$this->dbUser = $_sqli_conf['DB_USERNAME'];
				$this->dbPwd = $_sqli_conf['DB_PASSWORD'];
				if (isset($_sqli_conf['PORT'])) $this->dbPort = $_sqli_conf['PORT'];
			} else {
				$this->dbServer = $_PARAMS['DB_SERVER'];
				$this->dbName = $_PARAMS['DB_DATABASE'];
				$this->dbUser = $_PARAMS['DB_USERNAME'];
				$this->dbPwd = $_PARAMS['DB_PASSWORD'];
				if (isset($_PARAMS['PORT'])) $this->dbPort = $_PARAMS['PORT'];				
			} // end if params not sent
			if (isset($this->dbPort)){
				$db = new MySQLi($this->dbServer,$this->dbUser,$this->dbPwd,$this->dbName,$this->dbPort);
			}else{
				$db = new MySQLi($this->dbServer,$this->dbUser,$this->dbPwd,$this->dbName);
			}
	
			if (isset($db->connect_error)){
				$this->connect_errno = $db->connect_errno;
				$this->connect_error =  $db->connect_error;
			}else{
				$this->db = $db;
			}
		} // end constructor

		public function is_valid(){
			if ($this->connect_error){
				return false;
			}else{
				return true;
			} // end if connect_error
		}
		public function print_error(){
			printf("Connection failed: (%d) %s\n", $this->connect_errno, $this->connect_error);
		}
		public function getLink(){
			return $this->db;
		}
		public function add_row($sql){
			$myDb = $this->db;
			$result = false;
			$this->row_count = 0;
			$this->last_query = $sql;
			
			$result = $myDb->query($sql);
			
			$this->error_num = $myDb->errno;
			$this->error = $myDb->error;					
						
			if ( $result !== false ){
				$this->row_count = $myDb->affected_rows;
				$this->insert_id = $myDb->insert_id;
				
				return true;
			} else{

				$this->insert_id = NULL;
				return false;
			}
		}
		public function update_row($sql){
			$myDb = $this->db;
			$result = false;
			$this->row_count = 0;
			$this->insert_id = NULL;
			$this->last_query = $sql;

			if ($result = $myDb->query($sql)){
				$this->error_num = '';
				$this->error = '';
				$this->row_count = $myDb->affected_rows;
				
				return true;
			} else{
				$this->error_num = $myDb->errno;
				$this->error = $myDb->error;		
				return false;
			}
		}		
		public function delete_row($sql){
			$myDb = $this->db;
			$result = false;
			$this->last_query = $sql;
			if ($result = $myDb->query($sql)){
				$this->error_num = '';
				$this->error = '';
				return true;
			} else{
				$this->error_num = $myDb->errno;
				$this->error = $myDb->error;
				$this->row_count = 0;
				return false;
			}
		}
		public function get_results($sql){
			try{
				$myDb = $this->db;
				$result = false;			
				$this->last_query = $sql;
				if ($result = $myDb->query($sql)){
					$out = array();
					$this->error_num = '';
					$this->error = '';
					$this->row_count = $result->num_rows;
					while ($row = $result->fetch_assoc()) array_push($out,$row);
					$result->free();
					return $out;
				} else{
					$this->error_num = $myDb->errno;
					$this->error = $myDb->error;
					$this->row_count = 0;
					return false;				
				}		
			} catch (Exception $e) {
				die( 'Error: '.$e->getMessage()."\n");
			}
		} // end function get_results
	} // end class

} // end if class exists

?>