<?php

date_default_timezone_set(get_option('timezone_string'));

// remove more_announcements. We don't want them in this application
function giw_dequeue_script() {
	wp_dequeue_script( 'more_announcements' );
	wp_dequeue_script( 'kpit_alerts_ajax' );
	wp_dequeue_script( 'show_homepage_slides' );
	wp_dequeue_script( 'jqueryhoverintent' );
	wp_dequeue_script( 'kpamjqaccordion' );
}

add_action( 'wp_enqueue_scripts', 'giw_dequeue_script', 100 );

//Making jQuery Google API
function giw_modify_jquery() {
	if (!is_admin()) {
		// comment out the next two lines to load the local copy of jQuery
		wp_deregister_script('jquery');
		wp_register_script('jquery', ('http://'.$_SERVER['HTTP_HOST'].'/Utility/idevice/js/jquery-1.12.1.min.js'), false, '1.12.1');
		wp_enqueue_script('jquery');
	}
}
add_action('wp_enqueue_scripts', 'giw_modify_jquery');

// enqueue all javascripts universal to this application
function giw_enqueue_everything(){
	$server = $_SERVER['HTTP_HOST'];
	
	//**		enqueued stylesheets always appear in the page header     **//
	wp_enqueue_style('font-roboto', 'http://'.$server.'/Utility/idevice/css/font-roboto.css');
	wp_enqueue_style('datatables-jquery-ui', 'http://'.$server.'/Utility/idevice/js/DataTables/DataTables-1.10.11/css/dataTables.jqueryui.min.css');
	wp_enqueue_style('buttons-jquery-ui', 'http://'.$server.'/Utility/idevice/js/DataTables/Buttons-1.1.2/css/buttons.jqueryui.min.css');
	wp_enqueue_style('jquery-ui', 'http://'.$server.'/Utility/idevice/js/jquery-ui-1.11.4.custom/jquery-ui.css');
	wp_enqueue_style('bootstrap', 'http://'.$server.'/Utility/idevice/css/bootstrap.min.css');
	wp_enqueue_style('chosen', 'http://'.$server.'/Utility/idevice/js/chosen_v1.5.1/chosen.css');
	
	//**		enqueue scripts to appear in the page header     **//
	

	//**		enqueue scripts to appear in the page footer     **//

	// for use with dialog and other jQuery UI widgets
	wp_enqueue_script('jquery');
	wp_enqueue_script( 'bootstrap','http://'.$server.'/Utility/idevice/js/bootstrap.min.js', array('jquery'),'',false);
	wp_enqueue_script( 'jquery-ui','http://'.$server.'/Utility/idevice/js/jquery-ui-1.11.4.custom/jquery-ui.js',array('jquery'),'',false);
	wp_enqueue_script( 'datatables','http://'.$server.'/Utility/idevice/js/DataTables/DataTables-1.10.11/js/jquery.dataTables.min.js',array('jquery-ui'),'',false);

	wp_enqueue_script( 'datatables-jszip','http://'.$server.'/Utility/idevice/js/DataTables/JSZip-2.5.0/jszip.min.js', array('jquery'),'',false);
	wp_enqueue_script( 'datatables-pdfmake','http://'.$server.'/Utility/idevice/js/DataTables/pdfmake-0.1.18/build/pdfmake.min.js', array('jquery'),'',false);
	wp_enqueue_script( 'datatables-pdfmake-vfs-fonts','http://'.$server.'/Utility/idevice/js/DataTables/pdfmake-0.1.18/build/vfs_fonts.js', array('jquery'),'',false);
	wp_enqueue_script( 'datatables-jquery-ui','http://'.$server.'/Utility/idevice/js/DataTables/DataTables-1.10.11/js/dataTables.jqueryui.min.js', array('jquery'),'',false);
	wp_enqueue_script( 'datatables-buttons','http://'.$server.'/Utility/idevice/js/DataTables/Buttons-1.1.2/js/dataTables.buttons.min.js', array('jquery'),'',false);
	wp_enqueue_script( 'datatables-buttons-jquery-ui','http://'.$server.'/Utility/idevice/js/DataTables/Buttons-1.1.2/js/buttons.jqueryui.min.js', array('jquery'),'',false);
	wp_enqueue_script( 'datatables-buttons-colvis','http://'.$server.'/Utility/idevice/js/DataTables/Buttons-1.1.2/js/buttons.colVis.min.js', array('jquery'),'',false);
	wp_enqueue_script( 'datatables-buttons-flash','http://'.$server.'/Utility/idevice/js/DataTables/Buttons-1.1.2/js/buttons.flash.min.js', array('jquery'),'',false);
	wp_enqueue_script( 'datatables-buttons-html5','http://'.$server.'/Utility/idevice/js/DataTables/Buttons-1.1.2/js/buttons.html5.min.js', array('jquery'),'',false);
	wp_enqueue_script( 'datatables-buttons-print','http://'.$server.'/Utility/idevice/js/DataTables/Buttons-1.1.2/js/buttons.print.min.js', array('jquery'),'',false);
	wp_enqueue_script( 'datatables-responsive','http://'.$server.'/Utility/idevice/js/DataTables/Responsive-2.0.2/js/dataTables.responsive.min.js', array('jquery'),'',false);
	wp_enqueue_script( 'chosen','http://'.$server.'/Utility/idevice/js/chosen_v1.5.1/chosen.jquery.js', array('jquery'),'',false);


	// jquery cookie support
	wp_enqueue_script('jquery-cookie','http://'.$_SERVER['SERVER_NAME'].'/wp-content/themes/kaiser/assets/js/jquery.cookie.js',array('jquery'),'2.2.0',false);
	// user login support
	//wp_enqueue_script( 'logging','http://'.$server.'/Utility/gi-whiteboard/js/user_login.js',array('jquery'),'.5',true);

	
	//  NVLY field validation
	wp_enqueue_script( 'common_utils','http://'.($server=='localhost'?'cnrpwwebd500.rpw.ca.kp.org':$server).'/scripts/common_utils.min.js',array(),'1.5',true);

	/*	
	$js_root = 'http://'.$_SERVER['SERVER_NAME'].'/Utility/idevice/js/';
	$parent_path = dirname($_SERVER["SCRIPT_FILENAME"]);
	$separator = stristr($parent_path,'/') === false ? '\\' : '/';
	$path_parts = explode($separator, $parent_path);
	$filename = array_pop($path_parts);
	switch ($filename){
		case 'users':
			$js_file = 'giw_user_functions.js';
			break;
		case 'groups':
			$js_file = 'giw_groups_functions.js';
			break;
		default:
			$js_file = 'giw_functions.js';
			break;
	}
	*/
	wp_enqueue_script('local',$js_root.$js_file,array('jquery'),'2.2.0',true);

	
} // end function enqueue_everything
add_action('wp_enqueue_scripts', 'giw_enqueue_everything');

add_filter('body_class', 'giwb_body_classes');

function giwb_body_classes($classes) {
	if ( isset($classes['error404']) ) unset($classes['error404']);
    if ( isset($_SESSION['user']) ) {
		$classes[] = 'logged-in';
		$classes[] = 'single-author';
	}
	$classes[] = 'page';
	$classes[] = 'singular';
	$classes[] = 'giwb';
    return $classes;
}

function getMySQL_Today(){
	$phptime = strtotime('today midnight');
	return date ("Y-m-d H:i:s", $phptime);
}

get_header();
?>