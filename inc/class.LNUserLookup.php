<?php
/*************************************
/*	class.LNUserLookup.php
/*	PURPOSE:  Used to lookup and return
/*		basic user meta via LDAP connection
/*		to Lotus Notes.  Requires LN intranet
/*		password. Use of a fictitious account
/*		is recommended.
/*	AUTHOR:		Michael Baxter
/*				michael@kp.org
/*	ORGANIZATION:
/*		Operations Information Management
/*		North Sacramento Valley TPMG
/****************************************/
define("DEBUG", false	);

// 		Perform lookup of user info via Lotus Notes LDAP connection
//		and retrieve username, email, etc., using KP's NUID
if (!class_exists('LNUserLookup')){
	class LNUserLookup{
		// Class member variables
		//LDAP DEFINITIONS
		private $ldap = NULL;				//object to contain your ldap handle
		private $dn = 'CN=Michael Baxter, OU=CA, O=KAIPERM';	// Canonicalized Notes Username (ldap format)
		private $pwd = '';								// Admin password (sametime pwd) stored in plain text
		private $host = 'cscrdldap001.crdc.kp.org';				// fully canonical ldap server address
		private $port = 389;										// ldap port (retrieved from Notes Server Config document)
		private $sdn = 'OU=CA, O=KAIPERM';						// Canonical extension appended to user's name. Should match $dn above
		private $connected = false;								// boolean flag to store connection success / failure
		private $authenticated = false;							// boolean flag to store athentication success / failure
		
		function __construct(){
			// connect to Domino LDAP server
			if (!($this->ldap = ldap_connect($this->host, $this->port))) { 
				die ("Could not connect to LDAP server"); 
			} 
			$this->connected = true;
			
			// bind / authenticate using stored credentials
			if (!($res = @ldap_bind($this->ldap, $this->dn, $this->pwd))) { 
				die ("Could not bind as ".$this->dn); 
			} 
			$this->authenticated = true;
		} // end constuctor
		
		// NOTE:  when defining this object, you should check both no_connection() and 
		//	not_authorized() to ensure you're okay to proceed. Otherwise, you could
		//	encounter an unreported / undefined error.
		function no_connection(){
			if (DEBUG===true) echo "<p>checking connection</p>\n";
			$ret = ($this->connected == false ? true : false);
			return $ret;
		}
		function not_authorized(){
			if (DEBUG===true) echo "<p>checking authorization</p>\n";			
			$ret = ($this->authenticated == false ? true : false);
			return $ret;			
		}
		// when passed the employee NUID attempts to retrieve and return as ldap array.
		function get_employee_from_nuid($nuid=""){
			if (DEBUG===true) echo "<p>getting employee info for $nuid</p>\n";
			$filter = "(&(ObjectClass=Person)(shortName=*$nuid*))";
			$ret = $this->search_ldap($filter,true);
			return $ret;
		}
		// separated from search_ldap in case we need more search types, returns email as a string.
		function get_email_from_nuid($nuid = ""){
			if (DEBUG===true) echo "<p>getting email address for $nuid</p>\n";
			$filter = "(&(ObjectClass=Person)(shortName=*$nuid*))";
			$ret = $this->search_ldap($filter,false);
			return $ret;
		}
		// function search_ldap:  performs the search and returns the parsed values in a format indicated
		//	 by the parameters $filter and $allInfo;
		//	params:
		//		$filter:  ldap uses filters to tell it what information to search and how to match.
		//			in our case, we typically tell it to search "Person" documents, and match the shortName field
		//			to the NUID entered by the admin running the form.
		//		$allInfo (default=false): used to tell the function whether we want to parse for a string value
		//			email address or return the ldap array for additional parsing. If true, returns array with email, 
		//			firstname, and lastname.
		function search_ldap(&$filter,$allInfo=false){
			if (DEBUG===true) echo "<p>performing search with filter <br />\n$filter</p>\n";
			$ret = ''; // default return value is blank
			$ret_fields = array();
			$fields = ($allInfo===true ? array('shortName','mail','firstName','lastName') : array('shortName','mail'));
			
			if (DEBUG===true) { 
				echo "<p>search variables set &nbsp;";
				var_dump($fields);
				echo "</p>\n";
			} // end if DEBUG
			// Initialize search using global and local parameters			
			$sr = ldap_search($this->ldap, $this->sdn, $filter, $fields);
			if (DEBUG===true) echo "<p>ldap_search searched</p>\n";
			
			if ($sr === FALSE){
				if (DEBUG ===true) echo "<p>LDAP has failed to locate the NUID requested</p>\n";
				return '';
			}else{
				// Try to get any entries returned by the search
				$info = ldap_get_entries($this->ldap, $sr);
				if (DEBUG===true) {
					 echo "<p>ldap_get_entries successful</p>\n";
					 var_dump($info);
					 echo "<p>&nbsp;</p>\n";
				}
				
				
				if ($info === FALSE){
					if (DEBUG ===true) echo "<p>LDAP returned nothing</p>\n";
					return '';
				} else if( is_array($info) && array_key_exists('count',$info) && $info['count']==0){
					if (DEBUG ===true) echo "<p>NUID not found on LDAP server</p>\n";
					return '';
				}else{
					// if allInfo is requested, attempt to retrieve and parse value
					if ($allInfo == true){
						if (DEBUG===true){
							echo "<p><em>ALL</em> var array = ";
							var_dump($info);	
							echo "</p>\n";
						} // end if DEBUG
						// Strip the nsmtp from the email address. it is only needed for mail trf by Domino
						$ret_fields['em'] = str_replace('nsmtp.','',strtolower($info[0]['mail'][0]));
						$ret_fields['fname'] = ucfirst($info[0]['firstname'][0]);
						$ret_fields['lname'] = ucfirst($info[0]['lastname'][0]);
						if (DEBUG===true){
							echo "<p>fname = ";
							var_dump($info[0]['firstname']);
							echo "</p>\n<p>lname=";
							var_dump($info[0]['lastname']);
							echo "</p>\n<p>ret_fields =";
							var_dump($ret_fields);
							echo "</p>\n";
						} // end if debug
						if (DEBUG===true) die('Testing halted');
						return $ret_fields;
					} else {
						if (DEBUG===true) {
							echo "<p>Got email info &nbsp;";
							var_dump($info);
							echo "</p>\n";
						}
						$ret = str_replace('nsmtp.','',strtolower($info[0]['mail'][0]));
						if (DEBUG===true){
							echo "<p>email = ";
							var_dump($ret);	
						}
						return $ret;
					} // end if allInfo
				} // end if ldap_get_intries had a problem
			} // end if $sr is not FALSE
		} // end function search_ldap

	} // end class
} // end if class_exists

// declare / create new instance on 'Include' to use as a global object
// not ideal, but worked for the rush job.
$_LNUserLookup = new LNUserLookup();
if (DEBUG===true){
	echo "<p>The following is to verify connection status:</p>\n<ul>\n";
	switch (true){
		case ($_LNUserLookup->no_connection()):
			echo "<li>could not connect</li>";
		case ($_LNUserLookup->not_authorized()):
			echo "<li>failed authentication</li>";
		default:
			$email = $_LNUserLookup->get_email_from_nuid('w940344');
			echo "<li>".($email!=''? 'Got' : 'N0').' email for w940344'.($email!=''?"($email)":'')."</li>\n";
			break;
	} // end switch
	echo "</ul>\n";
}
?>